from pathlib import Path
import sys
import os
import shutil
import argparse
from types import SimpleNamespace

import docutils.io, docutils.core
import jinja2
from rst2html5_ import HTML5Writer

# TODO make into path
SRC = './data/'
DST = './public'

# TODO make dynamic
SITE_ROOT=""

# mostly taken from
# https://github.com/getpelican/pelican/
docutils_params = {
    'initial_header_level': '3',
    'syntax_highlight': 'short',
    'input_encoding': 'utf-8',
    'language_code': 'fr',
    'exit_status_level': 2,
    'embed_stylesheet': False,
    'math_output': 'MathJax'
}

parser = argparse.ArgumentParser(description='Generate the HTML site from sources.')
parser.add_argument('--site_root', default='')

template_env = jinja2.Environment(
    loader=jinja2.FileSystemLoader('.'),
    # TODO escaping
    # autoescape=jinja2.select_autoescape(['html', 'xml'])
)

def extract_metadata(publisher, allowed_categories=None):
    '''extract metadata from restructuredtext publisher'''

    metadata = dict()
    for docinfo in publisher.document.traverse(docutils.nodes.docinfo):
        for (key, value) in docinfo.children:
            key = key.astext()
            value = value.astext()

            if key == 'points':
                value = float(value)
                if value.is_integer():
                    value = int(value)
                metadata['points'] = value
            elif key == 'catégories':
                metadata['categories'] = [
                    # should handle ending semicolons properly
                    cat.strip() for cat in value.strip('; ').split(';')
                ]
                if allowed_categories:
                    for c in metadata['categories']:
                        if c not in allowed_categories:
                            raise Exception(f'unknown category "{c}"')
            else:
                raise Exception(f"unknown metadata key '{key}'")

    return metadata


def process_restructuredText(source_path, allowed_categories=None):
    # again, mostly taken from
    # https://github.com/getpelican/pelican/
    pub = docutils.core.Publisher(
        writer=HTML5Writer(),
        source_class=docutils.io.FileInput,
        destination_class=docutils.io.StringOutput)
    pub.set_components('standalone', 'restructuredtext', 'html5')
    pub.process_programmatic_settings(None, docutils_params, None)
    pub.set_source(source_path=str(source_path))
    pub.publish()
    
    try:
        metadata = extract_metadata(pub, allowed_categories)
    except Exception as e:
        msg = f'error while extracting the metadata of file {source_path}'
        raise Exception(msg) from e

    body = pub.writer.parts['body']

    return body, metadata
    
# TODO some arguments seem to always be there
# like 'title' etc.
# we could include them in the function signature
# to be able to pass them as positions parameters
def render_template(path_to_template, **template_params):
    template = template_env.get_template(path_to_template)

    return template.render(
            # XXX global variable
            site_root=SITE_ROOT,
            **template_params
    )

long_names = {
    'exam': {
        'brevet': 'Brevet des Collèges',
        'bac-s': 'Baccalauréat S'
    },
    'topic': {
        'math': 'Mathématiques',
    },
    'region': {
        'pondichéry': 'Pondichéry',
        'amérique-nord': 'Amérique du Nord',
        'centres-étrangers': 'Centres Étrangers'
    }
}

def parse_path(path):
    '''returns a human-readable representation (in the form of a list)
    of a path in the data file structure.

    :path: a pathlib.Path or a string

    >>> parse_path('brevet/math/2017/')
    ['Brevet des Collèges', 'Mathématiques', 2017]
    '''

    # in order to accept strings as well as paths
    path = Path(path)

    splitted = path.parts

    # in order to manage full paths (/home/cedric/...)
    # we look for the beginning of the interesting part of the path
    # that starts at the exam type (after '/data/')
    # XXX not very robust, for instance if 'brevet' appears upper in the path
    for exam_type in long_names['exam'].keys():
        if exam_type in splitted:
            start = splitted.index(exam_type)
            parts = splitted[start:]
            nb_parts = len(parts)
            break
    else:
        raise Exception(f'could not parse path {path}')

    result = list()

    result.append(long_names['exam'][parts[0]])

    if nb_parts > 1:
        result.append(long_names['topic'][parts[1]])

    if nb_parts > 2:
        result.append(int(parts[2]))

    if nb_parts > 3:
        result.append(long_names['region'][parts[3]])

    if nb_parts > 4:
        result.append(int(parts[4]))

    return result

CONTEXT_ITEM_FORMAT = '<li class="breadcrumb-item">{}</li>\n'
def build_context(levels=[]):
    '''builds the "context", that is, the nav bar to insert at the top of a page'''
    result = CONTEXT_ITEM_FORMAT.format(f'<a href="{SITE_ROOT}/">Accueil</a>')
    for level in levels:
        if isinstance(level, str):
            # level is just a string (no link)
            result += CONTEXT_ITEM_FORMAT.format(level)
        else:
            # level is a text and a href
            text = level['text']
            href = level['href']
            result += CONTEXT_ITEM_FORMAT.format(f'<a href="{href}">{text}</a>')

    return result

def process_exo(src, dst, allowed_categories):
    exam, topic, year, region, exo_number = parse_path(src)
    context = build_context(
        [
            {
                'text': f'{exam} {year} {topic}',
                'href': Path('../..'),
            },
            {
                'text': f'{region}',
                'href': Path('../tout/')
            }
        ]
    )


    instructions_body, metadata = process_restructuredText(src/'instructions.rst', allowed_categories)
    points = metadata['points']

    has_solution = True if (src / 'corrigé.rst').exists() else False

    instruction_html = render_template(
        'templates/exercice.html',
        context = context,
        title = (f'Exercice {exo_number} &mdash; '
                 f'{exam} {year} {topic} &mdash; {region}'),
        heading = (f'Exercice {exo_number} ({points} points)'),
        body = instructions_body,
        has_solution = has_solution
    )

    with open(dst/'index.html', 'w') as f:
        f.write(instruction_html)

    if has_solution:
        # no metadata from solution for now
        solution_body, _ = process_restructuredText(src/'corrigé.rst')

        solution_html = render_template(
            'templates/solution.html',
            instructions_body = instructions_body,
            solution_body = solution_body,
            context = context,
            title = (f'Exercice {exo_number} (Corrigé) &mdash; '
                     f'{exam} {year} {topic} &mdash; {region}'),
            heading = (f'Exercice {exo_number} (Corrigé)')
        )

        with open(dst/'corrigé.html', 'w') as f:
            f.write(solution_html)

    return metadata, has_solution

def process_entire_exam(src, dst):
    exam, topic, year, region = parse_path(src)
    context = build_context(
        [
            {
                'text': f'{exam} {year} {topic}',
                'href': Path('../..'),
            },
        ]
    )

    exo_list = list()
    for exo in src.iterdir():
        if exo.name == 'figures':
            continue

        instructions_body, metadata = process_restructuredText(exo/'instructions.rst')
        points = metadata['points']

        solution = (
            Path('..')/exo.name/'corrigé.html'
            if (exo/'corrigé.rst').exists()
            else None
        )
        
        exo_list.append({
            'exo_number': int(exo.name),
            'body': instructions_body,
            'points': points,
            'solution': solution
        })

    exo_list.sort(key=lambda exo:exo['exo_number'])

    title = f'{exam} {year} &mdash; {topic} &mdash; {region}'

    html = render_template(
        'templates/entire_exam.html',
        context = context,
        title = title,
        heading = title,
        exo_list = exo_list
    )

    os.mkdir(dst/'tout')
    with open(dst/'tout'/'index.html', 'w') as f:
        f.write(html)

def copy_images(src_dir, dst_dir):
    '''create the dst directory and copy images from the src_dir'''
    os.mkdir(dst_dir)
    for f in src_dir.iterdir():
        if f.suffix in ['.png', '.jpg', '.jpeg', '.svg']:
            # CAUTION in Python before 3.6 shutil expects strings
            shutil.copy(f, dst_dir)

def process_year(src, dst, allowed_categories):
    
    categories = list()

    index = dict()
    for region in src.iterdir():

        exo_list = list()
        for exo in region.iterdir():
            if exo.name == 'figures':
                continue

            exo_src_dir = exo
            exo_dst_dir = dst / region.name / exo.name
            os.makedirs(exo_dst_dir, exist_ok=True)

            metadata, has_solution = process_exo(exo_src_dir, exo_dst_dir, allowed_categories)

            exo_number = int(exo.name)
            points = metadata['points']

            index_item = {
                'url': f'{region.name}/{exo.name}',
                'description': (f"Exercice {exo_number} ({points} points)"),
                'solution': f'{region.name}/{exo.name}/corrigé.html' if has_solution else None,
                'categories': metadata.get('categories') or None
            }
            exo_list.append(index_item)

            if 'categories' in metadata:
                categories.append((exo, metadata['categories']))

        if (region/'figures').exists():
            copy_images(region/'figures', dst/region.name/'figures')

        exo_list.sort(key=lambda exo:exo['url'])

        # XXX confusing: here 'region' is the path to the directory for the current exam
        process_entire_exam(region, dst/region.name)

        region_long_name = long_names['region'][region.name]
        index[region_long_name] = {
            "entire_exam_href": Path(region.name)/'tout',
            "exercises": exo_list
        }

    exam, topic, year = parse_path(src)
    title = f'{exam} {year} {topic}'
    heading = title

    # context is just a link to home here
    context = build_context()
    
    html = render_template('templates/year_index.html',
                           title=title, context=context,
                           heading=heading, index=index)
    
    path_to_index = os.path.join(dst, 'index.html')
    with open(path_to_index, 'w') as f:
        f.write(html)

    return categories

def update_cat_idx(category_index, new_data):
    '''updates category index with new (exo, categories) tuples from process_year()'''
    for (exo, categories) in new_data:
        exam, topic, year, region, exo_number = parse_path(exo)

        entry = {
            'text': f'{exam} {year} {region} Exercice {exo_number}',
            # href from category index to exo:
            # just go up through 'catégories' and then rebuild the exo path
            'href': Path('..') / exo.relative_to(exo.parents[2])
        }

        for category in categories:
            if category not in category_index:
                category_index[category] = [entry]
            else:
                category_index[category] += [entry]

def render_category_index(topic, category_index, src, dst):
    cat_dst_dir = dst / topic.relative_to(src) / 'catégories'
    os.makedirs(cat_dst_dir)

    # XXX need a way to distinguish 'topic' as a path and 'topic' as a string
    exam_name, topic_name = parse_path(topic)
    context = build_context([f'{exam_name} {topic_name}'])

    for category in category_index:

        html = render_template('templates/category.html',
                               context = context,
                               category = category,
                               link_list = category_index[category],
                               title = f'Catégorie: { category }')

        with open(cat_dst_dir / (category + '.html'), 'w') as f:
            f.write(html)

def build_topic_category_list(category_index, topic, src):
    return [
        {
            'text': category,
            'href': topic.relative_to(src) / 'catégories' / ( category + '.html')
        }
        for category in category_index
    ]

def render_about_page(dst):
    '''takes HTML body from path './a-propos.html' and renders it with base template'''
    with open("a-propos.html") as f:
        body = f.read()

    html = render_template('templates/base.html', body=body,
                           context=build_context(),
                           title='À Propos', heading='À Propos')
    
    with open(dst/'a-propos.html', 'w') as f:
        f.write(html)

def main(src, dst):
    # emptying build directory
    # instead of removing/recreating it
    # because it would kill the server which PWD is the build directory
    dst = Path(dst)
    os.makedirs(dst, exist_ok=True)
    for each in dst.iterdir():
        if each.is_file():
            each.unlink()
        else:
            shutil.rmtree(each)

    index = dict()

    # XXX should I move this file in the 'data/' directory?
    with open('allowed_categories.txt') as f:
        allowed_categories = [line.strip() for line in f.readlines()]

    # TODO iterate from something else
    for exam in ['brevet']:
        exam = Path(src) / exam
        exam_index = dict()

        for topic in exam.iterdir():
            topic_index = dict()

            category_index = dict()

            for year in topic.iterdir():
                # 'year' is actually a path 'exam/topic/year' here
                year_categories = process_year(year, dst / year.relative_to(src), allowed_categories)
                topic_index[year.name] = f'{exam.name}/{topic.name}/{year.name}'
                update_cat_idx(category_index, year_categories)

            exam_index[long_names['topic'][topic.name]] = {
                'years': topic_index,
                'categories': build_topic_category_list(category_index, topic, src)
            }
            render_category_index(topic, category_index, src, dst)


        index[long_names['exam'][exam.name]] = exam_index

    html = render_template('templates/index.html', index=index,
                           title='Annales Brevet et Bac')
    with open(dst / 'index.html', 'w') as f:
        f.write(html)

    render_about_page(dst)

    shutil.copytree('css', dst / 'css')

if __name__ == '__main__':
    # XXX SITE_ROOT should be an argument of main()
    # should probably re-write it completely with an object
    print("processing...")
    args = parser.parse_args()
    SITE_ROOT = args.site_root
    main(SRC, DST)
    
