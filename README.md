Annales du brevet des collèges et du baccalauréat en format texte (reStructuredText).

- facilite la collaboration et les corrections
- utilisable par d'autres programmes
- licence ouverte (MIT pour l'instant)

**Contribuer**: voir [CONTRIBUTING.md][]
