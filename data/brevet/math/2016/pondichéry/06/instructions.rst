:points: 4
:catégories: pourcentages

Lors des soldes, Rami, qui accompagne sa mère et s’ennuie un peu,
compare trois étiquettes pour passer le temps :

.. image:: ../figures/exo-06-figure-01.svg

1. Quelle est le plus fort pourcentage de remise ?

2. Est-ce que la plus forte remise en euros est la plus forte en pourcentage ?
