:points: 4
:catégories: calcul; équations; 

Dans ce questionnaire à choix multiples,
pour chaque question,
des réponses sont proposées et une seule est exacte.

Pour chacune des questions, écrire le numéro de la question et la lettre de la bonne réponse.

Aucune justification n’est attendue.

+---------------------------------+------------------------+------------------------+------------------+
| Questions                       | Réponse A              | Réponse B              | Réponse C        |
+---------------------------------+------------------------+------------------------+------------------+
| 1: :math:`(2x−3)^2=` …          | :math:`4x^2 + 12x − 9` | :math:`4x^2 − 12x + 9` | :math:`4x^2 - 9` |
+---------------------------------+------------------------+------------------------+------------------+
|                                 | 1 et 2,5               | -1 et -2,5             | -1 et 2,5        |
| 2: L’équation                   |                        |                        |                  |
| :math:`(x + 1)(2x − 5) = 0`     |                        |                        |                  |
| a pour solutions …              |                        |                        |                  |
+---------------------------------+------------------------+------------------------+------------------+
|                                 | :math:`a`              | :math:`2\sqrt{a}`      | :math:`\sqrt{a}` |
| 3: Si :math:`a > 0` alors       |                        |                        |                  |
| :math:`\sqrt{a} + \sqrt{a} =` … |                        |                        |                  |
+---------------------------------+------------------------+------------------------+------------------+
