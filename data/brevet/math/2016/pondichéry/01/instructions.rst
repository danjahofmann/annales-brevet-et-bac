:points: 3
:catégories: conversion d'unités; problème;

Mélanie est une étudiante toulousaine qui vit en colocation dans un appartement.
Ses parents habitent à Albi et elle retourne chez eux les week-ends.
Elle rentre à Toulouse le dimanche soir.
Sur sa route, elle passe prendre ses 2 colocataires à la sortie n°3,
dernière sortie avant le péage.
Elle suit la route indiquée par l’application GPS de son téléphone portable,
dont l’affichage est reproduit ci-après.

.. image:: ../figures/exo-01-figure-01.svg

Elle est partie à 16h20
et entre sur l’autoroute au niveau de la sortie n°11 à 16h33.
Le rendez-vous est à 17h.
Sachant qu’il lui faut 3 minutes pour aller de la sortie n°3 au lieu de rendez-vous,
à quelle vitesse moyenne doit-elle rouler sur l’autoroute pour arriver à l’heure exacte ?
Vous donnerez votre réponse en km/h.

**Toute recherche même incomplète, sera valorisée dans la notation.**
