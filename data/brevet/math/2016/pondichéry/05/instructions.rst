:points: 5
:catégories: fonctions;

Lors d’une course en moto-cross,
après avoir franchi une rampe,
Gaëtan a effectué un saut record en moto.

.. image:: ../figures/exo-05-figure-01.svg

Le saut commence dès que Gaëtan quitte la rampe.

On note :math:`t` la durée (en secondes) de ce saut.

La hauteur (en mètres) est déterminée en fonction de la durée :math:`t`
par la fonction :math:`h` suivante :

.. math::

   h: t → ( -5t - 1,35 )( t - 3,7 )

Voici la courbe représentative de cette fonction h.

.. image:: ../figures/exo-05-figure-02.svg

Les affirmations suivantes sont-elles vraies ou fausses ?
Justifier en utilisant soit le graphique soit des calculs.

1. En développant et en réduisant l’expression de h
   on obtient
   
   .. math::
      
      h(t) = -5t^2 - 19,85 t – 4,995

2. Lorsqu’il quitte la rampe, Gaëtan est à 3,8 m de hauteur.

3. Le saut de Gaëtan dure moins de 4 secondes.

4. Le nombre 3,5 est un antécédent du nombre 3,77 par la fonction :math:`h`.

5. Gaetan a obtenu la hauteur maximale avant 1,5 seconde.
