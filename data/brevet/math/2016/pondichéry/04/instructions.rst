:points: 6
:catégories: géométrie; problème

L’inspecteur G. est en mission dans l’Himalaya.
Un hélicoptère est chargé de le transporter en haut d’une montagne
puis de l'amener vers son quartier général.

Le trajet ABCDEF modélise le plan de vol.
Il est constitué de déplacements rectilignes.
On a de plus les informations suivantes :

- AF= 12,5 km ; AC = 7,5 km ; CF = 10 km ; AB = 6 km ; DG = 7 km et EF = 750 m.
- (DE) est parallèle à (CF).
- ABCH et ABGF sont des rectangles

.. image:: ../figures/exo-04-figure-01.svg

.. image:: ../figures/exo-04-figure-02.svg

1. Vérifier que la longueur du parcours est de 21 kilomètres.

   **Dans cette question, toute trace de recherche sera valorisée.**

2. Le pilote doit-il avoir confiance en l’inspecteur G ?
   Justifier votre réponse.

