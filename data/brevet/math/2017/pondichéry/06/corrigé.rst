Pour tous les triangles,
on note A l'angle droit,
AB le dénivelé,
AC le déplacement horizontal
et BC la route.
La pente correspond donc à :math:`AB/AC`.

Pour la route du col du grand colombier,
on peut obtenir le déplacement horizontal AC
avec le théorème de Pythagore:

.. math::

   AB^2 + AC^2 = BC^2

   AC^2 = BC^2 - AB^2

   AC^2 = 1500^2 - 280^2

   AC^2 = 2171600

   AC = \sqrt{2171600} \approx 1474 \text{ m}


La pente est donc de :math:`AB/AC = 280/1474 \approx 0.19`
c'est à dire 19%.

Pour calculer la pente de l'Alto de l'Angliru,
on pourrait calculer le dénivelé AB
en utilisant la tangeante de l'angle connu :math:`\widehat{ACB}`.
Mais on s'apperçoit que cette tangeante est en fait tout simplement égale à la pente:

.. math::

   tan(\widehat{ACB}) = \frac{AB}{AC}

On a donc une pente de :math:`tan(\widehat{ACB}) = tan(12,4°) \approx 0.22`
soit 22%.

Le classement des pentes de la plus forte à le moins forte est donc:

- Château des Adhémar (24%)
- Alto de l'Angliru (22%)
- Col du Grand Colombier (19%)
