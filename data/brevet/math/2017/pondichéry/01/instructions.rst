:points: 5
:catégories: polynômes

On considère l'expression :math:`E = (x-2)(2x+3) - 3(x-2)`.

1. Développer :math:`E`.
2. Factoriser :math:`E` et vérifier que :math:`E = 2F`, où :math:`F = x(x-2)`
3. Déterminer tous les nombres :math:`x` tels que :math:`(x-2)(2x+3) - 3(x-2) = 0`
