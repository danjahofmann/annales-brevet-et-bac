:points: 7
:catégories: algorithmique

On considère le programme de calcul ci-contre dans lequel
x, Étape 1, Étape 2 et Résultat sont quatre variables.

.. image:: ../figures/exo-03-figure-01.png

1. a. Julie a fait fonctionner ce programme en choisissant le nombre 5.
      Vérifier que ce qui est dit à la fin est : « J’obtiens finalement 20 ».
   b. Que dit le programme si Julie le fait fonctionner en choisissant au départ le nombre 7 ?

2. Julie fait fonctionner le programme,
   et ce qui est dit à la fin est : « J’obtiens finalement 8 ».
   Quel nombre Julie a-t-elle choisi au départ ?

3. Si l’on appelle :math:`x` le nombre choisi au départ,
   écrire en fonction de :math:`x` l’expression obtenue à la fin du programme,
   puis réduire cette expression autant que possible.

4. Maxime utilise le programme de calcul ci-dessous :

   - Choisir un nombre
   - Lui ajouter 2
   - Multiplier le résultat par 5

   Peut-on choisir un nombre pour lequel
   le résultat obtenu par Maxime est le même que celui obtenu par Julie ?
