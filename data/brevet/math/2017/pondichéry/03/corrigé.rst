Question 1.a
---------------

Si Julie choisit 5,
alors :math:`x` vaut 5,
puis "Étape 1" vaut :math:`6×5 = 30`,
puis "Étape 2" vaut :math:`30 + 10 = 40`,
et finalement "Résultat" vaut :math:`40/2 = 20`.

Donc le programme dit effectivement "J'obtiens finalement 20".

Question 1.b
---------------

De la même façon avec :math:`x = 7`,
"Étape 1" vaut :math:`6×7 = 42`,
puis "Étape 2" vaut :math:`42 + 10 = 52`,
et finalement "Résultat" vaut :math:`52/2 = 26`.

Le programme dit donc "J'obtiens finalement 26".

Question 2
------------

Si le programme dit "J'obtiens finalement 8",
alors c'est que "Résultat" vaut 8,
donc "Étape 2" vaut 16 car :math:`16 / 2 = 8`,
donc "Étape 1" vaut 6 car :math:`6 + 10 = 16`,
et donc :math:`x` vaut 1 car :math:`6×1 = 6`

Donc Julie a choisi le nombre 1.

Question 3
------------

L'expression obtenue à la fin du programme est:

.. math::

   (6×x + 10) / 2

Qui peut être réduite en:

.. math::

   3×x + 5


Question 4
------------

On cherche un nombre :math:`x` tel que
le résultat de Julie soit le même que le résultat de Maxime,
c'est à dire tel que:

.. math::

   3x + 5 = (x + 2) × 5

On a alors:

.. math::

   3x + 5 =  5x + 10

   3x - 5x = 10 - 5

   -2x = 5

   x = -5/2 = -2,5

On peut donc avoir
le résultat de Maxime qui soit le même que celui obtenu par Julie
si on choisi :math:`-2,5`.
