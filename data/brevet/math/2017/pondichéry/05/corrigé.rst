Question 1.a
-----------------

La production totale d’électricité en France en 2014 est de
25,8 + 67,5 + 31 + 415,9 = 540,2 TWh.

Question 1.b
-----------------

La proportion d’électricité produite par les « Autres énergies (dont la géothermie) »
est de :math:`31 / 540,2 \approx 0.057386`
donc est bien environ égale à 5,7 %.

Question 2
-----------------

Tom pense qu’il s’agit des « Autres énergies (dont la géothermie) »
car cela correspond à la plus grosse augmentation en pourcentage (+10,3 %).

Alice pense qu’il s’agit du « Nucléaire »
car cela correspond à la plus grosse augmentation en quantité
(:math:`415,9 - 430,8 \approx 12` TWh).

Question 3.a
-----------------

Le volume du puits est de :math:`V = \frac{\pi}{3} × h × (R^2 + R×r + r^2)`
avec :math:`h = 2500` m, :math:`R = 0,23` m et :math:`r = 0,10` m.

On a donc:

.. math::

   V & = \frac{\pi}{3} × 2500 × ({0,23}^2 + 0,23×0,10 + {0,10}^2) \\
     & \approx 225 \text{ m}^3

Question 3.b
-----------------

Un volume de 225 m³ de terre correspond une fois extraite
à un volume de :math:`225 × 1,3 = 292.5 \text{ m}^3`
