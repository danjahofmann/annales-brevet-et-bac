:points: 8
:catégories: pourcentages; volumes;

Un TeraWattheure est noté : 1 TWh.

La géothermie permet la production d’énergie électrique grâce à la chaleur des nappes souterraines.
Le graphique ci-contre représente les productions d'électricité par différentes sources d'énergie en France en 2014.

.. image:: ../figures/exo-05-figure-01.png

1.
    a. Calculer la production totale d’électricité en France en 2014.
    b. Montrer que la proportion d’électricité produite par les « Autres énergies
       (dont la géothermie) » est environ égale à 5,7 %.
2. Le tableau suivant présente les productions d’électricité par les différentes
   sources d’énergie, en France, en 2013 et en 2014.

   +------------------+-----------+-------------+----------------------+-----------+
   |                  | Thermique | Hydraulique | Autres énergies      | Nucléaire |
   |                  | à Flamme  |             | (dont la géothermie) |           |
   +------------------+-----------+-------------+----------------------+-----------+
   | Production en    | 43,5      | 75,1        | 28,1                 | 403,8     |
   | 2013(en TWh)     |           |             |                      |           |
   +------------------+-----------+-------------+----------------------+-----------+
   | Production en    | 25,8      | 67,5        | 31                   | 415,9     |
   | 2014(en TWh)     |           |             |                      |           |
   +------------------+-----------+-------------+----------------------+-----------+
   | Variation de     | -40,7%    | -10,1%      | +10,3%               | +3%       |
   | production entre |           |             |                      |           |
   | 2013 et 2014     |           |             |                      |           |
   +------------------+-----------+-------------+----------------------+-----------+

   Alice et Tom ont discuté
   pour savoir quelle est la source d’énergie qui a le plus augmenté sa production d’électricité.
   Tom pense qu’il s’agit des « Autres énergies (dont la géothermie) »
   et Alice pense qu’il s’agit du « Nucléaire ».

   Quel est le raisonnement tenu par chacun d’entre eux ?

3. La centrale géothermique de Rittershoffen (Bas Rhin) a été inaugurée le 7 juin 2016.
   On y a creusé un puits pour capter de l’eau chaude sous pression,
   à 2500 m de profondeur, à une température de 170 degrés Celsius.

   Ce puits a la forme du tronc de cône représenté ci-contre.
   Les proportions ne sont pas respectées.

   .. image:: ../figures/exo-05-figure-02.svg

   On calcule le volume d’un tronc de cône grâce à la formule suivante :

   .. math::

      V = \frac{\pi}{3} × h × (R^2 + R×r + r^2)

   Où :math:`h` désigne la hauteur du tronc de cône,
   :math:`R` le rayon de la grande base
   et :math:`r` le rayon de la petite base.

   a. Vérifier que le volume du puits est environ égal à 225 m³.

   b. La terre est tassée quand elle est dans le sol.
      Quand on l’extrait, elle n’est plus tassée et son volume augmente de 30 %.

      Calculer le volume final de terre à stocker après le forage du puits.
