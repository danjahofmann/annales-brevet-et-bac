:points: 5
:catégories: conversion d'unités

Alban souhaite proposer sa candidature pour un emploi dans une entreprise.
Il doit envoyer dans une seule enveloppe :
2 copies de sa lettre de motivation et 2 copies de son Curriculum Vitæ (CV).
Chaque copie est rédigée sur une feuille au format A4.

1. Il souhaite faire partir son courrier en lettre prioritaire.
   Pour déterminer le prix du timbre,
   il obtient sur internet la grille de tarif d’affranchissement suivante :

   +-----------------------------+
   | Lettre Prioritaire          |
   +---------------+-------------+
   | Masse jusqu'à | Tarifs nets |
   +---------------+-------------+
   | 20 g          | 0,80 €      |
   +---------------+-------------+
   | 100 g         | 1,60 €      |
   +---------------+-------------+
   | 250 g         | 3,20 €      |
   +---------------+-------------+
   | 500 g         | 4,80 €      |
   +---------------+-------------+
   | 3 kg          | 6,40 €      |
   +---------------+-------------+

   Le tarif d’affranchissement est-il proportionnel à la masse d’une lettre ?

2. Afin de choisir le bon tarif d’affranchissement, il réunit les informations suivantes :

   - Masse de son paquet de 50 enveloppes : 175 g.
   - Dimensions d’une feuille A4 : 21 cm de largeur et 29,7 cm de longueur.
   - Grammage d’une feuille A4 : 80 g/m² (le grammage est la masse par m² de feuille).

   Quel tarif d’affranchissement doit-il choisir ?
