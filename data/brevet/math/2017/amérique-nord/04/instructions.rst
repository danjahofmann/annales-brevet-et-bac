:points: 10
:catégories: pourcentages; diagrammes;

Les données et les questions de cet exercice concernent la France métropolitaine.

Document 1
============

    En 2015, environ 4,7 % de la population française
    souffrait d’allergies alimentaires.
    En 2010, les personnes concernées par des allergies alimentaires
    étaient deux fois moins nombreuses qu’en 2015.
    En 1970, seulement 1 % de la population était concernée.

    *Source: Agence nationale de la sécurité sanitaire de l’alimentation,
    de l’environnement et du travail.*

Document 2
===========

.. figure:: ../figures/exo-04-figure-01.svg

Partie 1
===========

1. Déterminer une estimation du nombre de personnes, à 100 000 près,
   qui souffraient d’allergies alimentaires en France en 2010.
2. Est-il vrai qu’en 2015,
   il y avait environ 6 fois plus de personnes concernées qu’en 1970 ?

Partie 2
===========

En 2015, dans un collège de 681 élèves, 32 élèves souffraient d’allergies alimentaires.
Le tableau suivant indique les types d’aliments auxquels ils réagissaient.

+-----------------+------+--------+-----------+---------+------+
| Aliments        | Lait | Fruits | Arachides | Poisson | Oeuf |
+-----------------+------+--------+-----------+---------+------+
|                 | 6    | 8      | 11        | 5       | 9    |
| Nombre d'élèves |      |        |           |         |      |
| concernés       |      |        |           |         |      |
+-----------------+------+--------+-----------+---------+------+

1. La proportion des élèves de ce collège souffrant d’allergies alimentaires
   est-elle supérieure à celle de la population française ?

2. Jawad est étonné :
   « J’ai additionné tous les nombres indiqués dans le tableau et j’ai obtenu 39 au lieu de 32 ».
   Expliquer cette différence.

3. Lucas et Margot ont chacun commencé un diagramme
   pour représenter les allergies des 32 élèves de leur collège :

   .. figure:: ../figures/exo-04-figure-02.svg

   a) Qui de Lucas ou de Margot a fait le choix le mieux adapté à la situation ?
      Justifier la réponse.

   b) Reproduire et terminer le diagramme choisi à la question a)

