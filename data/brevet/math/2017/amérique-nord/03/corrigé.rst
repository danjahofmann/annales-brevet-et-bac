Question 1
================

Dans l'urne il y a 6 boules avec un numéro pair (2, 4, 6, 8, 10 et 12)
et 4 boules avec un numéro multiple de 3 (3, 6, 9 et 12).
Il est donc plus probable d'obtenir un numéro pair.


Question 2
================

Toutes les boules ont un numéro inférieur à 20,
donc on est certain d'obtenir un numéro inférieur à 20.
La probabilité est donc de 1.


Question 3
================

Les boules ayant un numéro diviseur de 6 sont les boules numéro 1, 2, 3 et 6.
Après les avoir retirées, il reste 3 boules avec un numéro premier (5, 7 et 11)
sur un total de 8 boules (puisqu'il y en avait 12 et qu'on en a retiré 4).

La probabilité de tirer une boule avec un nombre premier est donc de
:math:`\frac{3}{8} = 0,375` .
