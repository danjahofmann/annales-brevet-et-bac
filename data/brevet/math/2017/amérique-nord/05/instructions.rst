:points: 5
:catégories: algorithmique

L’image ci-dessous représente la position obtenue
au déclenchement du bloc départ d’un programme de jeu.

.. image:: ../figures/exo-05-figure-01.svg

L’arrière-plan est constitué de points espacés de 40 unités.
Dans cette position, le chat a pour coordonnées :math:`(−120 ; −80)`.
**Le but du jeu est de positionner le chat sur la balle.**

1. Quelles sont les coordonnées du centre de la balle représentée dans cette position ?

2. Dans cette question, le chat est dans la position obtenue au déclenchement du bloc départ.
   Voici le script du lutin « chat » qui se déplace.

   .. image:: ../figures/exo-05-figure-02.png

   a. Expliquez pourquoi le chat ne revient pas à sa position de départ
      si le joueur appuie sur la touche → puis sur la touche ←.

   b. Le joueur appuie sur la succession de touches suivante : → → ↑ ← ↓.
      Quelles sont les coordonnées :math:`x` et :math:`y` du chat après ce déplacement ?

   c. Parmi les propositions de succession de touches ci-dessous,
      laquelle permet au chat d’atteindre la balle ?

      +---------------+---------------+---------------+
      | Déplacement 1 | Déplacement 2 | Déplacement 3 |
      +---------------+---------------+---------------+
      | →→→→→→→↑↑↑↑↑  | →→→↑↑↑→↓←     | ↑→↑→↑→→↓↓     |
      +---------------+---------------+---------------+

3. Que se passe-t-il quand le chat atteint la balle ?
