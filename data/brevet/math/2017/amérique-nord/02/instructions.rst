:points: 9.5
:catégories: géométrie

Avec un logiciel de géométrie, on éxécute le programme ci dessous.

**programme de construction:**

- Construire un carré ABCD;
- Tracer le cercle de centre A de rayon [AC];
- Placer le point E à l'intersection du cercle
  et de la demi-droite [AB);
- Construire un carré DEFG.

**Figure obtenue:**

.. image:: ../figures/exo-02-figure-01.svg

1. Sur la copie, réaliser la construction avec **AB = 3 cm**.

2. Dans cette question, **AB = 10 cm**.

   a. Montrer que AC = :math:`\sqrt{200}` cm.

   b. Expliquer pourquoi AE = :math:`\sqrt{200}` cm.

   c. Montrer que l'aire du carré DEFG est le triple de l'aire du carré ABCD.

3. On admet pour cette question qu pour n'importe quelle longueur du côté [AB],
   l'aire du carré DEFG est toujours le triple de l'aire du carré ABCD.
   En exécutant ce programme de construction,
   on souhaite obtenir un carré DEFG ayant un aire de 48 cm².

   Quelle longueur AB faut-il choisir au départ ?
