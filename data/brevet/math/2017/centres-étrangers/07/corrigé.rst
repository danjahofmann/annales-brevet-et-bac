Question 1
======================

La cuisine de Bob a une surface de :math:`4 × 5 = 20` m².
Le vendeur lui indique qu'il faut commander 5 % de carrelage en plus
c'est à dire :math:`20 × \frac{5}{100} = 1` m² de carrelage en plus.

Bob doit donc commander au moins 21 m² de carrelage.


Question 2
======================

Chaque paquet permet de recouvrir 1,12 m² de carrelage
et :math:`21 / 1,12 \approx 18,75`,
Bob doit donc acheter 19 paquets de carrelage.


Question 3
======================

19 paquets de carrelage coûteront :math:`19 × 31 = 589` € à Bob.


Question 4
======================


+----------------+----------+------------------+---------------+
| Matériaux      | Quantité |                  |               |
|                |          | Montant Unitaire | Montant Total |
|                |          | Hors Taxe        | Hors Taxe     |
+----------------+----------+------------------+---------------+
| Sceau de colle | 3        | 12€              | 36€           |
+----------------+----------+------------------+---------------+
|                | 1        | 7€               | 7€            |
| Sachet de      |          |                  |               |
| croisillons    |          |                  |               |
+----------------+----------+------------------+---------------+
|                | 2        | 22,5€            | 45€           |
| Sac de joints  |          |                  |               |
| pour carrelage |          |                  |               |
+----------------+----------+------------------+---------------+
|                |          | TOTAL HORS TAXE  | 88€           |
+----------------+----------+------------------+---------------+
|                |          | TVA (20%)        | 17,6€         |
+----------------+----------+------------------+---------------+
|                |          |                  | 105,6€        |
|                |          | TOTAL TOUTES     |               |
|                |          | TAXES COMPRISES  |               |
+----------------+----------+------------------+---------------+
