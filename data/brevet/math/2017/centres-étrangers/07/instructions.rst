:points: 7
:catégories: conversion d'unités; calcul;

Bob doit refaire le carrelage de sa cuisine
dont la forme au sol est un rectangle de 4 m par 5 m.

Il a choisi son carrelage dans un magasin.
Le vendeur lui indique qu'il faut commander 5 % de carrelage en plus
pour compenser les pertes dues aux découpes.

Le carrelage choisi se vend dans des paquets
permettant de recouvrir 1,12 m² et chaque paquet coûte 31 €.

1. Montrer que Bob doit commander au moins 21 m² de carrelage.

2. Combien doit-il acheter de paquets de carrelage ?

3. Quel sera le coût de l’achat du carrelage de sa cuisine ?

4. Bob se rend ensuite dans un autre magasin pour acheter le reste de ses matériaux.

   *Compléter la facture ci-dessous la joindre à la copie.*

   +----------------+----------+------------------+---------------+
   | Matériaux      | Quantité |                  |               |
   |                |          | Montant Unitaire | Montant Total |
   |                |          | Hors Taxe        | Hors Taxe     |
   +----------------+----------+------------------+---------------+
   | Sceau de colle | 3        | 12€              | 36€           |
   +----------------+----------+------------------+---------------+
   |                | ...      | 7€               | ...           |
   | Sachet de      |          |                  |               |
   | croisillons    |          |                  |               |
   +----------------+----------+------------------+---------------+
   |                | 2        | ...              | 45€           |
   | Sac de joints  |          |                  |               |
   | pour carrelage |          |                  |               |
   +----------------+----------+------------------+---------------+
   |                |          | TOTAL HORS TAXE  | 88€           |
   +----------------+----------+------------------+---------------+
   |                |          | TVA (20%)        | ...           |
   +----------------+----------+------------------+---------------+
   |                |          |                  | ...           |
   |                |          | TOTAL TOUTES     |               |
   |                |          | TAXES COMPRISES  |               |
   +----------------+----------+------------------+---------------+
