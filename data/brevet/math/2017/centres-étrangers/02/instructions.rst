:points: 7
:catégories: fonctions; calcul;

Partie 1
==================

Pour réaliser une étude sur différents isolants,
une société réalise 3 maquettes de maison strictement identiques
à l’exception près des isolants qui diffèrent dans chaque maquette.
On place ensuite ces 3 maquettes dans une chambre froide réglée à 6°C.
On réalise un relevé des températures
ce qui permet de construire les 3 graphiques suivants :

.. image:: ../figures/exo-02-figure-01.svg

.. image:: ../figures/exo-02-figure-02.svg

.. image:: ../figures/exo-02-figure-03.svg

1. Quelle était la température des maquettes
   avant d'être mise dans la chambre froide ?

2. Cette expérience a-t-elle duré plus de 2 jours ?
   Justifier votre réponse.

3. Quelle est la maquette qui contient l'isolant le plus performant ?
   Justifier votre réponse.

Partie 2
==================

Pour respecter la norme RT2012 des maisons BBC (Bâtiments Basse Consommation),
il faut que la résistance thermique des murs notée :math:`R`
soit supérieure ou égale à 4.
Pour calculer cette résistance thermique, on utilise la relation :

.. math::

   R = \frac{e}{c}

où :math:`e` désigne l'épaisseur de l'isolant en mètre
et :math:`c` désigne le coefficient de conductivité thermique de l'isolant.
Ce coefficient permet de connaître la performance de l'isolant.

1. Noa a choisi comme isolant la laine de verre
   dont le coefficient de conductivité thermique est : :math:`c = 0,035`.
   Il souhaite mettre 15 cm de laine de verre sur ses murs.

   Sa maison respecte-t-elle la norme RT2012 des maisons BBC ?

2. Camille souhaite obtenir une résistance thermique de 5 (:math:`R = 5`).
   Elle a choisi comme isolant du liège
   dont le coefficient de conductivité thermique est : :math:`c = 0,04`.

   Quelle épaisseur d'isolant doit-elle mettre sur ses murs ?
