:points: 6
:catégories: géométrie; volumes

Voici les dimensions de quatre solides :

- Une pyramide de 6 cm de hauteur
  dont la base est un rectangle de 6 cm de longueur
  et de 3 cm de largeur.
- Un cylindre de 2 cm de rayon et de 3 cm de hauteur.
- Un cône de 3 cm de rayon et de 3 cm de hauteur.
- Une boule de 2 cm de rayon.

1. a. Représenter approximativement les trois premiers solides comme l’exemple ci-dessous :

      .. image:: ../figures/exo-03-figure-01.svg

   b. Placer les dimensions données sur les représentations.

2. Classer ces quatre solides dans l’ordre croissant de leur volume.

Quelques formules :

.. math::

   \frac{4}{3} × π × \text{rayon}^3

   π × \text{rayon}^2 × \text{hauteur}

   \frac{1}{3} × π × \text{rayon}^2 × \text{hauteur}

   \frac{1}{3} × \text{aire de la base} × \text{hauteur}
