Puisqu'un espace de 20 cm doit être laissé 
entre la surface de l’eau et le haut de la piscine,
le volume d'eau à atteindre est celui
d'un pavé droit de 1,60 m de profondeur seulement
(les longueur et largeurs restent les même que celles de la piscine).

Cela représente un volume d'eau de :math:`8 × 4 × 1,60 = 51.2` m³,
soit 51 200 L.

Comme 10 L sont remplis en 18 secondes,
le temps nécessaire pour remplir la piscine est de

.. math::

   18 × \frac{51\ 200}{10} = 92\ 160 \text{ s}

Une journée compte 24 heures et une heure 3 600 secondes,
une journée compte donc :math:`24 × 3\ 600 = 86\ 400` secondes.

Il faut donc effectivement plus d'une journée pour remplir la piscine.
