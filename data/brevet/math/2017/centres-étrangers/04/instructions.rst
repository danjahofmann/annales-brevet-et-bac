:points: 4
:catégories: tableur; probabilités

Un fabricant de volets roulants électriques réalise une étude statistique
pour connaître leur fiabilité.
Il fait donc fonctionner un échantillon de 500 volets sans s’arrêter,
jusqu’à une panne éventuelle.
Il inscrit les résultats dans le tableur ci-dessous :

.. image:: ../figures/exo-04-figure-01.png

1. Quelle formule faut-il saisir dans la cellule H2 du tableur
   pour obtenir le nombre total de volets testés ?

2. Un employé prend au hasard un volet dans cet échantillon.
   Quelle est la probabilité que ce volet fonctionne plus de 3000 montées descentes ?

3. Le fabricant juge ses volets fiables
   si plus de 95 % des volets fonctionnent plus de 1000 montées descentes.
   Ce lot de volets roulants est-il fiable ? Expliquer votre raisonnement.
