Question 1
====================

La formule à saisir dans la cellule H2 est: ``=Somme(B2:G2)``


Question 2
====================

Sur un total de :math:`20 + 54 + 137 + 186 + 84 + 19 = 500` volets testés,
le nombre de volets ayant fonctionné plus de 3000 montées-descentes est de
:math:`186 + 84 + 19 = 289`.

La probabilité qu'un volet pris au hasard
fonctionne plus de 3000 montées-descentes est donc de:

.. math::

   \frac{289}{500} \approx 0,578


Question 3
====================

Le nombre de volets fonctionnant plus de 1000 montées-descentes est de
:math:`54 + 137 + 186 + 84 + 19 = 480`,
c'est à dire :math:`\frac{480}{500} = 0.96 = 96\%`.

Ce lot de volets roulants est donc fiable
car plus de 95% fonctionnent plus de 1000 montées-descentes.
