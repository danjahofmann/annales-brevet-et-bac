:points: 9
:catégories: algorithmique; géométrie; problème

Pour tracer une « rue », on a défini le tracé d'une « maison ».

.. image:: ../figures/exo-06-figure-01.png

.. image:: ../figures/exo-06-figure-02.svg

1. Vérifier que :math:`d` est environ égal à 71 à l’unité près.

2. Un point dans une fenêtre d’exécution de votre programme
   a son abscisse qui peut varier de :math:`-240` à :math:`240`
   et son ordonnée qui peut varier de :math:`-180` à :math:`180`.

   Quel est le plus grand nombre entier :math:`n`
   que l'on peut utiliser dans le programme principal
   pour que le tracé de la « rue »
   tienne dans la fenêtre de votre ordinateur où s'exécute le programme ?

   *Vous pourrez tracer sur votre copie tous les schémas (à main levée ou non)
   qui auront permis de répondre à la question précédente
   et ajouter toutes les informations utiles
   (valeurs, codages, traits supplémentaires, noms de points...)*

3. *Attention, cette question est indépendante des questions précédentes
   et la « maison » est légèrement différente.*

   Si on désire rajouter une sortie de cheminée au tracé de la maison
   pour la rendre plus réaliste,
   il faut faire un minimum de calculs pour ne pas avoir de surprises.

   Exemples:

   .. image:: ../figures/exo-06-figure-03.svg

   On suppose que :

   - les points H, E et A sont alignés ;
   - les points C, M et A sont alignés ;
   - [CH] et [EM] sont perpendiculaires à [HA] ;
   - AM = 16 ;
   - MC = 10 ;
   - :math:`\widehat{HAC}` = 30° .

   .. image:: ../figures/exo-06-figure-04.svg

   Calculer EM, HC et HE
   afin de pouvoir obtenir une belle sortie de cheminée.
