Question 1
=======================

Le sommet de la maison est un angle de 90°,
donc le triangle formé par le toit est un triangle rectangle
et :math:`d` est la longueur de l'hypothénuse de ce triangle.

On peut donc calculer :math:`d` en utilisant le thérorème de Pythagore
puisqu'on connait la longeur de chacun des autres côtés (50 « pas » chacun):

.. math::

   d = \sqrt{50^2 + 50^2} \approx 70.7107

:math:`d` est donc bien égal à 71 à l'unité près.


Question 2
=======================

:math:`n` est le nombre de « maisons » que l'on dessine.
Chaque maison a une largeur de :math:`d \approx 71`,
et après une maison vient une séparation de largeur 20
(le bloc « avancer de 20 pas »).

La « rue » occupe donc une largeur de :math:`n × (71 + 20) = 91 × n`.

La largeur maximale de la rue est de :math:`240 × 2 = 480`
(abscisse variant de -240 à +240).

Pour obtenir le plus grand :math:`n` possible,
on fait la division euclidienne de 480 par 91:

.. math::

   480 = 5 × 91 + 25

La plus grande valeur de :math:`n`
tel que le tracé de la rue tienne dans la fenêtre
est donc de 5
(et il restera 25 « pas » de largeur inutilisé).


Question 3
=======================

[EM] est perpendiculaire à [HA],
donc le triangle MAE est rectangle en E
et on a:

.. math::

   \sin(\widehat{MAE}) &= \frac{EM}{MA} \\
   EM &= MA × \sin(\widehat{MAE}) \\
   EM &= 16 × \sin(30°) \\
   EM &= 8

De même, [CH] est perpendiculaire à [HA]
donc le triangle HAC est rectangle en H
et on a:

.. math::
   \sin(\widehat{CAH}) &= \frac{HC}{CA} \\
   HC &= CA × \sin(\widehat{CAH}) \\
   &= (CM + MA) × \sin(\widehat{CAH}) \\
   &= (10 + 16) × 1/2 \\
   HC &= 13

Enfin pour calculer HE on remarque que HE = HA - EA,
or HA et EA peuvent être calculés de la même manière que HC et EM:

.. math::

   \cos(\widehat{HAC}) &= \frac{HA}{CA} \\
   HA &= CA × \cos{\widehat{HAC}} \\
   HA &= (10 + 16) × \cos{30°} \\
   HA &\approx 22,52

.. math::

   \cos(\widehat{EAM}) &= \frac{EA}{MA} \\
   EA &= MA × \cos(\widehat{EAM}) \\
   EA &= 16 × \cos{30°} \\
   EA &\approx 13,86

On a donc :math:`HE \approx 22,52 - 13,86 \approx 8,66`
