Affirmation 1
-------------------

On a:

.. math::

   BC^2 = 97^2 = 9409

et

.. math::

   BA^2 + AC^2 &= 65^2 + 72^2 \\
               &= 4225 + 5184 \\
               &= 9409

Donc :math:`BC^2 = BA^2 + AC^2`,
donc par la réciproque du théorème de Pythagore,
le triangle ABC est rectangle en A.

L'étagère a donc bien un angle droit.


Affirmation 2
-------------------

Le triangle CAH est rectangle en H,
donc le cosinus de l'angle :math:`\widehat{CAH}`
peut se calculer comme ceci:

.. math::

   cos(\widehat{CAH}) = \frac{AH}{AC} = \frac{5}{6}

L'angle :math:`\widehat{CAH}` est donc égal à
:math:`acos(\frac{5}{6}) \approx 34°`.

La pente du toit est donc bien entre 30° et 35° et la construction respecte bien la norme.


Affirmation 3
-------------------

3 couches de peintures sur 4 paires de volets (donc 8 volets) fait 3×8=24 couches, si chaque couche demande ⅙ de pot de peinture, le nombre de pots de peinture nécessaires est de

.. math::

   24 × \frac{1}{6} = \frac{24}{6} = 4

Et non 2 comme l'affirme le peintre.
