:points: 9
:catégories: géométrie

1. a. Tracer un triangle CDE rectangle en D tel que CD = 6,8 cm et DE = 3,4 cm .
   b.  Calculer CE au dixième de cm près.
   
2. a. Placer le point F sur [CD] tel que CF = 2 cm.
   b. Placer le point G sur [CE] tel que FG = 1 cm.
   c. Les droites (FG) et (DE) sont-elles parallèles ?