# Contribuer

Le principal objectif de ce projet est de faciliter la collaboration,
donc toute contribution est bienvenue,
et vous pouvez contribuer de nombreuses façons:

- **si vous savez "coder" ou vous voulez apprendre**:
vous pouvez contribuer directement au projet en faisant des *merge request*
(l'équivalent des *pull request* sur GitHub,
voir [GitLab doc: merge requests](https://docs.gitlab.com/ee/user/project/merge_requests/index.html)).
Le format à respecter pour les fichiers (dans le répertoire `data`)
devrait être assez évident en regardant les fichiers existants,
mais une spécification est disponible (complétée petit-à-petit)
sur [le wiki du projet](https://gitlab.com/cedricvanrompay/annales-brevet-et-bac/wikis/).

- **si vous n'êtes pas à l'aise avec le système de merge request**:
vous pouvez toujours juste recopier un exo ou proposer une correction
et la proposer via une *issue* sur le projet GitLab:  
https://gitlab.com/cedricvanrompay/annales-brevet-et-bac-data/issues/new  
Cela demande tout de même de créer un compte (gratuit) sur GitLab.

- vous pouvez aussi créer des *issues* simplement pour signaler une erreur,
suggérer une modification, faire un commentaire, etc...
